export const ganKinhLenManHinh = (glassArray) => {
    const glassList = document.getElementById('vglassesList');
    glassArray.forEach((glass) => {
        let newDiv = document.createElement('div');
        let newImg = document.createElement('img');
        newImg.src = glass.src;
        newImg.alt = glass.name;
        newImg.classList.add("w-100");
        newDiv.append(newImg);
        newDiv.classList.add("col-4");
        newDiv.addEventListener('click', () => {
            ganKinhLenKhuonMat(glass);
        })
        glassList.append(newDiv);
    })
}
const ganKinhLenKhuonMat = (glass) => {
    xuLyKinh(glass);
    xuLyThongTinKinh(glass);
}
const xuLyKinh = (glass) => {
    let khuonMat = document.getElementById("avatar");
    removeGlassesInfoChild(khuonMat);
    ganKinhLenAvatar(glass, khuonMat);
}
const ganKinhLenAvatar = (glass, khuonMat) => {
    let newDiv = document.createElement('div');
    let newImg = document.createElement('img');
    newImg.src = glass.virtualImg;
    newImg.alt = glass.name;
    newDiv.append(newImg);
    khuonMat.append(newDiv);
}
const xuLyThongTinKinh = (glass) => {
    let glassesInfo = document.getElementById('glassesInfo');
    let header = xuLyHeaderInfo(glass);
    let main = xuLyMainInfo(glass);
    let footer = xuLyFooterInfo(glass);
    removeGlassesInfoChild(glassesInfo);
    glassesInfo.append(header);
    glassesInfo.append(main);
    glassesInfo.append(footer);
    glassesInfo.classList.add('d-block');

}
const removeGlassesInfoChild = (divNeedToRemove) => {
    let child = divNeedToRemove.lastElementChild;
    while (child) {
        divNeedToRemove.removeChild(child);
        child = divNeedToRemove.lastElementChild;
    }
}
const xuLyHeaderInfo = (glass) => {
    let headerInfo = document.createElement('div');
    let name = document.createElement('span');
    name.textContent = glass.name.toUpperCase() + " - ";
    let brand = document.createElement('span');
    brand.textContent = glass.brand + " ";
    let color = document.createElement('span');
    color.textContent = `(${glass.color})`;
    headerInfo.append(name);
    headerInfo.append(brand);
    headerInfo.append(color);
    headerInfo.classList.add("font-weight-bold");
    return headerInfo;
}
const xuLyMainInfo = (glass) => {
    let mainInfo = document.createElement('div');
    let price = document.createElement('span');
    price.classList.add('bg-danger');
    price.classList.add('px-2');
    price.classList.add('py-1');
    price.classList.add('d-inline-block');
    price.classList.add('rounded');

    price.textContent = `$${glass.price}`;
    let something = document.createElement('span');
    something.classList.add('text-success');
    something.textContent = " stocking";
    mainInfo.append(price);
    mainInfo.append(something);
    return mainInfo;
}
const xuLyFooterInfo = (glass) => {
    let footerInfo = document.createElement('div');
    let des = document.createElement('span');
    des.textContent = glass.description;
    footerInfo.append(des);
    return footerInfo;
}

